package com.sprinklebit.qa.tests.acceptance.posts

import com.codeborne.selenide.Selenide
import com.sprinklebit.qa.pagemodel.DashboardPage
import com.sprinklebit.qa.pagemodel.HomePage
import com.sprinklebit.qa.tests.BaseTest
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

class CreatePostTest extends BaseTest {

    private DashboardPage dashboardPage

    @BeforeMethod
    void handlingPreconditions() {
        Selenide.open("/")
        dashboardPage = new HomePage()
                .openLoginModal()
                .login(testData.authentication.username, testData.authentication.password)
    }

    @AfterMethod
    void cleanAfterTest() {
        dashboardPage
                .removeLatestPost()
    }

    @Test
    void userCanSubmitPost() {
        dashboardPage = dashboardPage
                .createNewPost(testData.post.text)
                .verifyLastCreatedPost(testData.profile.name, testData.post.text)
    }
}
