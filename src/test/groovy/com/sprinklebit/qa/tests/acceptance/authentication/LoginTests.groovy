package com.sprinklebit.qa.tests.acceptance.authentication

import com.codeborne.selenide.Selenide
import com.sprinklebit.qa.pagemodel.HomePage
import com.sprinklebit.qa.tests.BaseTest
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

class LoginTests extends BaseTest {

    @BeforeMethod
    void openApplication() {
        Selenide.open("/")
    }

    @Test
    void userCanLogin() {
        new HomePage()
                .openLoginModal()
                .login(testData.authentication.username, testData.authentication.password)
                .verifyLoggedInUser(testData.profile.name)
    }
}
