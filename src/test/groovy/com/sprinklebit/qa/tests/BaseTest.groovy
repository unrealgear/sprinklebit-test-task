package com.sprinklebit.qa.tests

import com.codeborne.selenide.Configuration
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeSuite

import static com.codeborne.selenide.Selenide.clearBrowserCookies
import static com.codeborne.selenide.Selenide.executeJavaScript
import static com.codeborne.selenide.WebDriverRunner.clearBrowserCache

/**
 * Parent class which contains common variables, methods, and configurations for all test.
 */
abstract class BaseTest {

    /**
     * In order not to spend time for implementing test data reading mechanism, the test data will be stored here. But
     * I would suggest using YAML files to store test data which will allow us to implement Data Driven Testing.
     */
    protected Map testData = [authentication: [username: "test1",
                                               password: "123456"],
                              profile       : [name: "test test"],
                              post          : [text: "Simple text"]]

    /**
     * Sets Selenide global configuration properties. Static configuration of Selenide is thread safe. They are set as a
     * global parameters per suite before starting parallel threads.
     */
    @BeforeSuite
    void initSelenideConfiguration() {
        System.setProperty("wdm.targetPath", "${System.getProperty("user.dir")}/src/test/resources/webdrivers")
        Configuration.browser = "chrome" // sets name of a browser to run the tests in
        Configuration.baseUrl = "https://qa.sprinklebit.com" // sets base url for the application under the test
    }

    /**
     * Handles test method post-conditions, cleans browser state before the next execution.
     */
    @AfterMethod
    void logout() {
        clearBrowserCache()
        clearBrowserCookies()
        //clearBrowserLocalStorage() - disabled due to the issue in ChromeDriver v2.40
        executeJavaScript("sessionStorage.clear();")
    }
}
