package com.sprinklebit.qa.pagemodel

import com.codeborne.selenide.SelenideElement
import com.sprinklebit.qa.pagemodel.modals.LoginModal

import static com.codeborne.selenide.Selenide.$

/**
 * Represents the home page of the application.
 */
class HomePage {

    private SelenideElement loginButton = $("a.open-login-form")

    LoginModal openLoginModal() {
        loginButton.click()
        new LoginModal()
    }
}
