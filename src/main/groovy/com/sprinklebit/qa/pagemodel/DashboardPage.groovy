package com.sprinklebit.qa.pagemodel


import com.codeborne.selenide.ElementsCollection
import com.codeborne.selenide.Selenide
import com.codeborne.selenide.SelenideElement

import static com.codeborne.selenide.Condition.text
import static com.codeborne.selenide.Condition.visible
import static com.codeborne.selenide.Selenide.$
import static com.codeborne.selenide.Selenide.$$

/**
 * Represents the dashboard page of the application.
 */
class DashboardPage {

    private SelenideElement userFullName = $(".compact-profile-name"),
                            mainPostField = $(".main_form textarea"),
                            mainPostSubmitButton = $(".main_form div[ng-click='onSubmitClicked()']")

    private ElementsCollection feedPosts = $$(".wall_entry")

    DashboardPage createNewPost(String text) {
        enterPostText(text)
        submitPost()
    }

    DashboardPage removeLatestPost() {
        // waitUntil is added as there is a delay in appearing of the delete button
        feedPosts.get(0).find("[ng-click='deleteEntry()'] span")
                .waitUntil(visible, 4000).click()

        Selenide.confirm()
        this
    }

    DashboardPage verifyLoggedInUser(String fullName) {
        userFullName.shouldHave(text(fullName))
        this
    }

    DashboardPage verifyLastCreatedPost(String fullName, String postText) {
        SelenideElement postEntry = feedPosts.get(0)

        // verify full name
        findPostAuthor(postEntry).shouldHave(text(fullName))

        // verify post text
        findPostText(postEntry).shouldHave(text(postText))
        this
    }

    DashboardPage verifyThatThereIsNoPost(String fullName, String postText) {

        feedPosts.find {
            findPostAuthor(it).text != fullName && findPostText(it).text != postText
        }

        this
    }

    private DashboardPage enterPostText(String text) {
        mainPostField.val(text)
        this
    }

    private DashboardPage submitPost() {
        mainPostSubmitButton.click()
        this
    }

    private SelenideElement findPostAuthor(SelenideElement post) {
        post.find("[bind-html-compile='entry.partials.html_title'] a:nth-child(1)")
    }

    private SelenideElement findPostText(SelenideElement post) {
        post.find(".entry_content_text > ng-bind-html.feed-post-html-content")
    }
}
