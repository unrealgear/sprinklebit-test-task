package com.sprinklebit.qa.pagemodel.modals

import com.codeborne.selenide.Condition
import com.codeborne.selenide.SelenideElement
import com.sprinklebit.qa.pagemodel.DashboardPage

import static com.codeborne.selenide.Selenide.$
import static com.codeborne.selenide.Selenide.sleep

/**
 * Represents the login modal which is opened from the header.
 */
class LoginModal {

    private SelenideElement usernameField = $("#_username"),
                            passwordField = $("#_password"),
                            submitButton = $("#login-btn")

    /**
     * For current tests, only login method is public as we do not check negative tests cases (for example entering
     * only username and trying to submit).
     */
    DashboardPage login(String username, String password) {
        enterUsername(username)
        enterPassword(password)
        submit()
    }

    private LoginModal enterUsername(String username) {
        usernameField.val(username)

        // it a workaround because Selenium works faster than the page reacts to the input.
        sleep(400)

        this
    }

    private LoginModal enterPassword(String password) {
        passwordField.val(password)
        this
    }

    /**
     * This method does not allow us to handle negative test cases. In order to handle them, I would make tryToSubmit
     * method which will not return an instance of another page.
     */
    private DashboardPage submit() {
        submitButton.click()
        new DashboardPage()
    }
}
